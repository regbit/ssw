package root;

public class Token implements InstructionUnit {

	//Token types
	public static final int IF = 0, TRUE = 1, FALSE = 2, INT = 3, BOOL = 4, VARIABLE = 5, CONSTANT = 6, END_OF_LINE = 7, COMPARE_SIGN = 8, COMPARE_BOOLEAN_SIGN = 9,
			ASSIGN = 10, MATH_SIGN_HIGH = 11, MATH_SIGN_LOW = 12, BOOLEAN_AND = 13, BOOLEAN_OR = 14, BOOLEAN_NOT = 15,
			LEFT_BRACKET = 16, RIGHT_BRACKET = 17, LEFT_BRACE = 18, RIGHT_BRACE = 19;
	
	private int type;
	private String value;
	
	public Token(int type, String value) {
		this.type = type;
		this.value = value;
	}

	public int getType() {
		return type;
	}
	
	public String getTypeName() {
		if(type == IF) {
			return "IF";
		} else if(type == TRUE) {
			return "TRUE";
		} else if(type == FALSE) {
			return "FALSE";
		} else if(type == INT) {
			return "INT";
		} else if(type == BOOL) {
			return "BOOL";
		} else if(type == VARIABLE) {
			return "VARIABLE";
		} else if(type == CONSTANT) {
			return "CONSTANT";
		} else if(type == END_OF_LINE) {
			return "END_OF_LINE";
		} else if(type == COMPARE_SIGN) {
			return "COMPARE_SIGN";
		} else if(type == COMPARE_BOOLEAN_SIGN) {
			return "COMPARE_BOOLEAN_SIGN";
		} else if(type == ASSIGN) {
			return "ASSIGN";
		} else if(type == MATH_SIGN_HIGH) {
			return "MATH_SIGN_HIGH";
		} else if(type == MATH_SIGN_LOW) {
			return "MATH_SIGN_LOW";
		} else if(type == BOOLEAN_AND) {
			return "BOOLEAN_AND";
		} else if(type == BOOLEAN_OR) {
			return "BOOLEAN_OR";
		} else if(type == BOOLEAN_NOT) {
			return "BOOLEAN_NOT";
		} else if(type == LEFT_BRACKET) {
			return "LEFT_BRACKET";
		} else if(type == RIGHT_BRACKET) {
			return "RIGHT_BRACKET";
		} else if(type == LEFT_BRACE) {
			return "LEFT_BRACE";
		} else if(type == RIGHT_BRACE) {
			return "RIGHT_BRACE";
		} else {
			return "NULL";
		}
	}

	public String getValue() {
		return value;
	}
}
