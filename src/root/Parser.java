package root;

import java.util.ArrayList;

public class Parser {
	
	private Lexer lxr;
	private ArrayList<InstructionUnit> units;
	private boolean err = false;
	
	public Parser(Lexer lxr) {
		this.lxr = lxr;
		units = new ArrayList<InstructionUnit>(lxr.getTokens().size());
		for(Token t : lxr.getTokens()) {
			units.add(t);
		}
		if(!lxr.isErr()) {
			analyze();
		}
	}
	
	private void analyze() {
		System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println("Beginning of Parser");
		
		//Complicated math solving
		int option = 0;
		while(option != 4) {
			boolean sign = false, high = false, low = false, brackets = false;
			if(option == 0) {
				for(int i = 0; i < units.size() - 2; i++) {
					sign = sign || checkSign(i);
				}
				
				if(!sign) {
					option = 1;
				} else {
					option = 0;
				}
			} else if(option == 1) {
				for(int i = 0; i < units.size() - 2; i++) {
					high = high || checkMathHigh(i);
				}
				
				if(!high) {
					option = 2;
				} else {
					option = 0;
				}
			} else if(option == 2) {
				for(int i = 0; i < units.size() - 2; i++) {
					low = low || checkMathLow(i);
				}
				
				if(!low) {
					option = 3;
				} else {
					option = 0;
				}
			} else if(option == 3) {
				for(int i = 0; i < units.size() - 2; i++) {
					brackets = brackets || checkMathBrackets(i);
				}
				
				if(!brackets) {
					option = 4;
				} else {
					option = 0;
				}
			}
		}
		
		//Complicated boolean solving
		option = 0;
		while(option != 5) {
			boolean boolNot = false, boolAnd = false, boolOr = false, bool = false, brackets = false;
			if(option == 0) {
				for(int i = 0; i < units.size() - 2; i++) {
					boolNot = boolNot || checkBooleanNot(i);
				}
				
				if(!boolNot) {
					option = 1;
				} else {
					option = 0;
				}
			} else if(option == 1) {
				for(int i = 0; i < units.size() - 2; i++) {
					boolAnd = boolAnd || checkBooleanAnd(i);
				}
				
				if(!boolAnd) {
					option = 2;
				} else {
					option = 0;
				}
			} else if(option == 2) {
				for(int i = 0; i < units.size() - 2; i++) {
					boolOr = boolOr || checkBooleanOr(i);
				}
				
				if(!boolOr) {
					option = 3;
				} else {
					option = 0;
				}
			} else if(option == 3) {
				for(int i = 0; i < units.size() - 2; i++) {
					bool = bool || checkBoolean(i);
				}
				
				if(!bool) {
					option = 4;
				} else {
					option = 0;
				}
			} else if(option == 4) {
				for(int i = 0; i < units.size() - 2; i++) {
					brackets = brackets || checkBooleanBrackets(i);
				}
				
				if(!brackets) {
					option = 5;
				} else {
					option = 0;
				}
			}
		}

		boolean flag = true;
		flag = true;
		while(flag) {
			flag = false;
			for(int j = 0; j < units.size() - 2; j++) {
				flag = flag || checkAssignment(j);
			}
		}
		
		flag = true;
		while(flag) {
			flag = false;
			for(int j = 0; j < units.size() - 2; j++) {
				flag = flag || checkVariableInit(j);
			}
		}
		
		flag = true;
		while(flag) {
			flag = false;
			for(int j = 0; j < units.size() - 1; j++) {
				flag = flag || checkMethod(j);
			}
		}
		
		flag = true;
		while(flag) {
			flag = false;
			for(int j = 0; j < units.size() - 1; j++) {
				flag = flag || checkStatement(j);
			}
		}
		
		flag = true;
		while(flag) {
			flag = false;
			for(int j = units.size() - 5; j >= 0; j--) {
				flag = flag || checkCondition(j);
			}
			for(int j = units.size() - 2; j >= 0; j--) {
				flag = flag || checkBody(j);
			}
		}
		
		System.out.println("End of Parser");
		System.out.println("----------------------------------------------------------");
		if(!err) {
			System.out.println("Units:");
			for(InstructionUnit unit : units) {
				System.out.println("Terminal " + unit.getTypeName() + " \"" + unit.getValue() + "\"");
			}
		} else {
			System.out.println("PARSER ERROR");
			System.out.println("Unknown: ");
			for(InstructionUnit t : units) {
				if(t.getClass() == Token.class) {
					System.out.println("\"" + t.getTypeName() + "\" ");
				}
			}
		}
		System.out.println();
	}
	
	private boolean checkSign(int num) {
		if(units.get(num).getType() != Token.VARIABLE && units.get(num).getType() != Token.CONSTANT 
				&& units.get(num).getType() != Token.RIGHT_BRACKET && units.get(num).getType() != Terminal.MATH) {
			if(units.get(num + 1).getType() == Token.MATH_SIGN_LOW) {
				if(units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Token.CONSTANT || units.get(num + 2).getType() == Terminal.MATH) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
					//unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					//units.remove(num);
					units.remove(num + 1);
					units.set(num + 1, new Terminal(Terminal.MATH, unitSequence));
					System.out.println("--V--HERE--V--");
					System.out.println("MATH \"" + units.get(num+1).getValue() + "\"");
					System.out.println("--^--HERE--^--");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkMathHigh(int num) {
		if(units.get(num).getType() == Token.VARIABLE || units.get(num).getType() == Token.CONSTANT || units.get(num).getType() == Terminal.MATH) {
			if(units.get(num + 1).getType() == Token.MATH_SIGN_HIGH) {
				if(units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Token.CONSTANT || units.get(num + 2).getType() == Terminal.MATH) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.MATH, unitSequence));
					System.out.println("MATH \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkMathLow(int num) {
		if(units.get(num).getType() == Token.VARIABLE || units.get(num).getType() == Token.CONSTANT || units.get(num).getType() == Terminal.MATH) {
			if(units.get(num + 1).getType() == Token.MATH_SIGN_LOW) {
				if(units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Token.CONSTANT || units.get(num + 2).getType() == Terminal.MATH) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.MATH, unitSequence));
					System.out.println("MATH \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkMathBrackets(int num) {
		if(units.get(num).getType() == Token.LEFT_BRACKET) {
			if(units.get(num + 1).getType() == Terminal.MATH || units.get(num + 1).getType() == Token.VARIABLE || units.get(num + 1).getType() == Token.CONSTANT) {
				if(units.get(num + 2).getType() == Token.RIGHT_BRACKET) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.MATH, unitSequence));
					System.out.println("MATH \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkBooleanNot(int num) {
		if(units.get(num).getType() == Token.BOOLEAN_NOT) {
			if(units.get(num + 1).getType() == Token.TRUE || units.get(num + 1).getType() == Token.FALSE || units.get(num + 1).getType() == Token.VARIABLE || units.get(num + 1).getType() == Terminal.BOOLEAN) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
				System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
				return true;
			}
		}
		return false;
	}
	
	private boolean checkBooleanAnd(int num) {
		if(units.get(num).getType() == Token.TRUE || units.get(num).getType() == Token.FALSE || units.get(num).getType() == Token.VARIABLE || units.get(num).getType() == Terminal.BOOLEAN) {
			if(units.get(num + 1).getType() == Token.BOOLEAN_AND) {
				if(units.get(num + 2).getType() == Token.TRUE || units.get(num + 2).getType() == Token.FALSE || units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Terminal.BOOLEAN) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
					System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkBooleanOr(int num) {
		if(units.get(num).getType() == Token.TRUE || units.get(num).getType() == Token.FALSE || units.get(num).getType() == Token.VARIABLE || units.get(num).getType() == Terminal.BOOLEAN) {
			if(units.get(num + 1).getType() == Token.BOOLEAN_OR) {
				if(units.get(num + 2).getType() == Token.TRUE || units.get(num + 2).getType() == Token.FALSE || units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Terminal.BOOLEAN) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
					System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkBoolean(int num) {
		if(units.get(num).getType() == Token.VARIABLE || units.get(num).getType() == Token.CONSTANT || units.get(num).getType() == Terminal.MATH) {
			if(units.get(num + 1).getType() == Token.COMPARE_SIGN || units.get(num + 1).getType() == Token.COMPARE_BOOLEAN_SIGN) {
				if(units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Token.CONSTANT || units.get(num + 2).getType() == Terminal.MATH) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
					System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		if(units.get(num).getType() == Token.TRUE || units.get(num).getType() == Token.FALSE || units.get(num).getType() == Token.VARIABLE) {
			if(units.get(num + 1).getType() == Token.COMPARE_BOOLEAN_SIGN) {
				if(units.get(num + 2).getType() == Token.TRUE || units.get(num + 2).getType() == Token.FALSE || units.get(num + 2).getType() == Token.VARIABLE) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
					System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;		
	}
	
	private boolean checkBooleanBrackets(int num) {
		if(units.get(num).getType() == Token.LEFT_BRACKET) {
			if(units.get(num + 1).getType() == Terminal.BOOLEAN || units.get(num + 1).getType() == Token.VARIABLE || units.get(num + 1).getType() == Token.TRUE || units.get(num + 1).getType() == Token.FALSE) {
				if(units.get(num + 2).getType() == Token.RIGHT_BRACKET) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.BOOLEAN, unitSequence));
					System.out.println("BOOLEAN \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkAssignment(int num) {
		if(units.get(num).getType() == Token.VARIABLE) {
			if(units.get(num + 1).getType() == Token.ASSIGN) {
				if(units.get(num + 2).getType() == Token.VARIABLE || units.get(num + 2).getType() == Token.CONSTANT || units.get(num + 2).getType() == Terminal.MATH || units.get(num + 2).getType() == Terminal.BOOLEAN) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(3);
					unitSequence.add(units.get(num));
					unitSequence.add(units.get(num + 1));
					unitSequence.add(units.get(num + 2));
					units.remove(num);
					units.remove(num);
					units.set(num, new Terminal(Terminal.ASSIGNMENT, unitSequence));
					System.out.println("ASSIGNMENT \"" + units.get(num).getValue() + "\"");
					return true;
				}
			}
		}
		return false;
	}
	
	private boolean checkVariableInit(int num) {
		if(units.get(num).getType() == Token.INT) {
			if(units.get(num + 1).getType() == Terminal.ASSIGNMENT) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.VARIABLE_INIT, unitSequence));
				System.out.println("VARIABLE_INIT \"" + units.get(num).getValue() + "\"");
				return true;
			} else if(units.get(num + 1).getType() == Token.VARIABLE) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.VARIABLE_INIT, unitSequence));
				System.out.println("VARIABLE_INIT \"" + units.get(num).getValue() + "\"");
				return true;
			}
		} else if(units.get(num).getType() == Token.BOOL) {
			if(units.get(num + 1).getType() == Terminal.ASSIGNMENT) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.VARIABLE_INIT, unitSequence));
				System.out.println("VARIABLE_INIT \"" + units.get(num).getValue() + "\"");
				return true;
			} else if(units.get(num + 1).getType() == Token.VARIABLE) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.VARIABLE_INIT, unitSequence));
				System.out.println("VARIABLE_INIT \"" + units.get(num).getValue() + "\"");
				return true;
			}
		}
		return false;
	}
	
	private boolean checkMethod(int num) {
		if(units.get(num).getType() == Token.VARIABLE) {
			if(units.get(num + 1).getType() == Token.LEFT_BRACKET) {
				for(int i = num + 2; i < units.size(); i++) {
					if(units.get(i).getType() == Token.RIGHT_BRACKET) {
						ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(i + 1);
						for(int j = num; j < i + 1; j++) {
							unitSequence.add(units.get(num));
						}
						for(int j = num; j < i; j++) {
							units.remove(num);
						}
						units.set(num, new Terminal(Terminal.METHOD, unitSequence));
						System.out.println("METHOD \"" + units.get(num).getValue() + "\"");
						return true;
					}
				}
			}
			if(units.get(num + 1).getType() == Terminal.MATH) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.METHOD, unitSequence));
				System.out.println("METHOD \"" + units.get(num).getValue() + "\"");
				return true;
			}
		}
		return false;
	}
	
	private boolean checkStatement(int num) {
		if(units.get(num).getType() == Terminal.ASSIGNMENT || units.get(num).getType() == Terminal.VARIABLE_INIT || units.get(num).getType() == Terminal.METHOD) {
			if(units.get(num + 1).getType() == Token.END_OF_LINE) {
				ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(2);
				unitSequence.add(units.get(num));
				unitSequence.add(units.get(num + 1));
				units.remove(num);
				units.set(num, new Terminal(Terminal.STATEMENT, unitSequence));
				System.out.println("STATEMENT \"" + units.get(num).getValue() + "\"");
				return true;
			}
		}
		return false;
	}
	
	private boolean checkBody(int num) {
		if(units.get(num).getType() == Token.LEFT_BRACE) {
			for(int i = num + 1; i < units.size(); i++) {
				if(units.get(i).getType() == Token.RIGHT_BRACE) {
					ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(i + 1);
					for(int j = num; j < i + 1; j++) {
						unitSequence.add(units.get(j));
					}
					for(int j = num; j < i; j++) {
						units.remove(num);
					}
					units.set(num, new Terminal(Terminal.BODY, unitSequence));
					System.out.println("BODY \"" + units.get(num).getValue() + "\"");
					return true;
				} else if(units.get(i).getClass() == Token.class) {
					System.out.println("Err: " + units.get(i).getTypeName());
					err = true;
					return false;
				}
			}
		}
		return false;
	}
	
	private boolean checkCondition(int num) {
		if(units.get(num).getType() == Token.IF) {
			if(units.get(num + 1).getType() == Token.LEFT_BRACKET) {
				if(units.get(num + 2).getType() == Terminal.BOOLEAN) {
					if(units.get(num + 3).getType() == Token.RIGHT_BRACKET) {
						if(units.get(num + 4).getType() == Terminal.BODY) {
							ArrayList<InstructionUnit> unitSequence = new ArrayList<InstructionUnit>(5);
							unitSequence.add(units.get(num));
							unitSequence.add(units.get(num + 1));
							unitSequence.add(units.get(num + 2));
							unitSequence.add(units.get(num + 3));
							unitSequence.add(units.get(num + 4));
							units.remove(num);
							units.remove(num);
							units.remove(num);
							units.remove(num);
							units.set(num, new Terminal(Terminal.CONDITION, unitSequence));
							System.out.println("CONDITION \"" + units.get(num).getValue() + "\"");
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public Lexer getLexer() {
		return lxr;
	}

	public void setLexer(Lexer lxr) {
		this.lxr = lxr;
	}

	public boolean isErr() {
		return err;
	}
	
	public InstructionUnit getInstruction(int i) {
		if(i < units.size()) {
			return units.get(i);
		}
		return null;
	}
	
	public ArrayList<InstructionUnit> getInstructions() {
		return units;
	}
}
