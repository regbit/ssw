package root;

import java.util.ArrayList;

public class Terminal implements InstructionUnit {
	
	//Terminal types
	public static final int MATH = 100, BOOLEAN = 101, ASSIGNMENT = 102, VARIABLE_INIT = 103, STATEMENT = 104, BODY = 105, METHOD = 106, CONDITION = 107;
	
	private int type;
	private ArrayList<InstructionUnit> units;
	
	public Terminal(int type, ArrayList<InstructionUnit> units) {
		this.type = type;
		this.units = units;
	}

	public int getType() {
		return type;
	}
	
	public String getTypeName() {
		if(type == MATH) {
			return "MATH";
		} else if(type == BOOLEAN) {
			return "BOOLEAN";
		} else if(type == ASSIGNMENT) {
			return "ASSIGNMENT";
		} else if(type == VARIABLE_INIT) {
			return "VARIABLE_INIT";
		} else if(type == STATEMENT) {
			return "STATEMENT";
		} else if(type == BODY) {
			return "BODY";
		} else if(type == METHOD) {
			return "METHOD";
		} else if(type == CONDITION) {
			return "CONDITION";
		} else {
			return "NULL";
		}
	}
	
	public String getValue() {
		String value = "";
		for(InstructionUnit unit : units) {
			value = value + unit.getValue();
		}
		return value;
	}

	public InstructionUnit getUnit(int i) {
		if(i < units.size()) {
			return units.get(i);
		}
		return null;
	}

	public ArrayList<InstructionUnit> getUnits() {
		return units;
	}
}
