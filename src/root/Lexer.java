package root;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Lexer {
	
	private String line;
	private ArrayList<Token> tokens;
	private boolean err;
	
	//Temp
	private Pattern ifPtr = Pattern.compile("if");
	private Pattern truePtr = Pattern.compile("true");
	private Pattern falsePtr = Pattern.compile("false");
	private Pattern intPtr = Pattern.compile("int");
	private Pattern boolPtr = Pattern.compile("bool");
	private Pattern varPtr = Pattern.compile("[a-zA-Z]{1}[a-zA-Z0-9]*");
	private Pattern constPtr = Pattern.compile("([1-9]{1}[0-9]*([.][0-9]*)?)|0");
	private Pattern endPtr = Pattern.compile(";");
	private Pattern compPtr = Pattern.compile("[><]=?");
	private Pattern boolCompPtr = Pattern.compile("!=|==");
	private Pattern assignPtr = Pattern.compile("=");
	private Pattern mathHighPtr = Pattern.compile("[\\*\\/\\^]");
	private Pattern mathLowPtr = Pattern.compile("[\\+\\-]");
	private Pattern boolAnd = Pattern.compile("(\\&\\&)");
	private Pattern boolOr = Pattern.compile("(\\|\\|)");
	private Pattern boolNot = Pattern.compile("!");
	private Pattern lbPtr = Pattern.compile("\\("), rbPtr = Pattern.compile("\\)");
	private Pattern lsbPtr = Pattern.compile("\\{"), rsbPtr = Pattern.compile("\\}");
	private Pattern spacePtr = Pattern.compile("\\s+");
	
	private Matcher m;
	
	private ArrayList<Pattern> ptrList = new ArrayList<Pattern>(20);
	
	public Lexer(String line) {
		this.line = line;
		tokens = new ArrayList<Token>(20);
		
		//Key Words
		ptrList.add(ifPtr);
		ptrList.add(truePtr);
		ptrList.add(falsePtr);
		ptrList.add(intPtr);
		ptrList.add(boolPtr);
		//Variables and constants
		ptrList.add(varPtr);
		ptrList.add(constPtr);
		//Signs
		ptrList.add(endPtr);
		ptrList.add(compPtr);
		ptrList.add(boolCompPtr);
		ptrList.add(assignPtr);
		ptrList.add(mathHighPtr);
		ptrList.add(mathLowPtr);
		ptrList.add(boolAnd);
		ptrList.add(boolOr);
		ptrList.add(boolNot);
		ptrList.add(lbPtr);
		ptrList.add(rbPtr);
		ptrList.add(lsbPtr);
		ptrList.add(rsbPtr);
		//Space check. Always the last
		ptrList.add(spacePtr);
		analyze();
	}
	
	private void analyze() {
		System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println("Beginning of Lexer");
		String analyzing, unanalyzed = line;
		boolean flag = false, last = false, space = false;
		int to = 0, step = 0, lastMatchNum = 999;
		while(!last) {
			last = unanalyzed.length() == 1;
			to++;
			boolean match = false;
			System.out.println("#" + step);
			analyzing = unanalyzed.substring(0, to);
			System.out.println("Analyzing \"" + analyzing + "\"");
			System.out.println("To: " + to);
			for(int i = 0; i < ptrList.size(); i++) {
				Pattern ptr = ptrList.get(i);
				m = ptr.matcher(analyzing);
				match = match || m.matches();
				lastMatchNum = m.matches() && (i != ptrList.size() - 1) && i < lastMatchNum ? i : lastMatchNum;
				if(m.matches()) {
					System.out.println("Match: " + i);
				}
				if(i == ptrList.size() - 1 && m.matches()) {
					System.out.println("Space");
					space = true;
				}
			}
			
			if(flag && !match) {
				if(!space) {
					tokens.add(new Token(lastMatchNum, unanalyzed.substring(0, to - 1)));
					System.out.println("Token added: \"" + unanalyzed.substring(0, to - 1) + "\" as " + lastMatchNum);
					lastMatchNum = 999;
				} else {
					space = false;
				}
				unanalyzed = unanalyzed.substring(to - 1, unanalyzed.length());
				System.out.println("New unanalyzed: \"" + unanalyzed + "\"");
				to = 0;
			}
			if((flag && last) || (match && last)) {
				tokens.add(new Token(lastMatchNum, unanalyzed.substring(0, to)));
				lastMatchNum = 999;
				System.out.println("Token added: \"" + unanalyzed.substring(0, to) + "\"");
				System.out.println("End of Lexer");
				to = 0;
			} 
			if(!flag && !match && last) {
				err = true;
				break;
			}
			
			flag = match; //Keeps this cycle match info for the next cycle
			
			step++;
			System.out.println();
		}

		System.out.println("----------------------------------------------------------");
		if(!err) {
			System.out.println("Tokens:");
			for(Token tkn : tokens) {
				System.out.print("Token " + tkn.getTypeName() +" \"" + tkn.getValue() + "\" \n");
			}
		} else {
			System.out.println("LEXER ERROR");
		}
		System.out.println("");
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public ArrayList<Token> getTokens() {
		return tokens;
	}

	public Token getToken(int i) {
		if(i < tokens.size()) {
			return tokens.get(i);
		} else {
			System.out.println("Array index out of bounds: " + i);
			return null;
		}
	}

	public boolean isErr() {
		return err;
	}
}
