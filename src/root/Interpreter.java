package root;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Interpreter {
	
	private Lexer lexer;
	private Parser parser;
	
	private HashMap<String, Integer> intVars;
	private HashMap<String, Boolean> boolVars;
	
	private HashMap<Terminal, Integer> mathRes;
	private HashMap<Terminal, Boolean> boolRes;
	
	public Interpreter() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a proper Java based code: ");
		
		//String line = "int a = 2; int b = 5; int c = 3; c = a + b; int d = c + a; int e; e = (30 - (20 - (6 + 4 * 2 + 2 * 3))); println(c);"; //Interpreter test
		//String line = "if(a > b) { if(bool == true){c = -36.6 + d; e = f >= 10;} println(b);}"; //Parser test
		//String line = "int a = -(3+(4+5)+6+5-(5-6-(-7-2))) / 3;"; //Math example 
		String line = "bool a = !((true || false) && (true && (false || 5 > 6)));"; //Math example 
		//String line = sc.nextLine(); //Self-typed Java code
		
		System.out.println("Your line: \"" + line + "\"");
		
		lexer = new Lexer(line);
		parser = new Parser(lexer);
		
		intVars = new HashMap<String, Integer>();
		boolVars = new HashMap<String, Boolean>();
		
		mathRes = new HashMap<Terminal, Integer>();
		boolRes = new HashMap<Terminal, Boolean>();
		
		System.out.println("||||||||||||||||||||||||||||||||||||||||||||||||||||||||||");
		System.out.println("Beginning of Interpreter");
		execute(parser.getInstructions());
	}
	
	public void execute(ArrayList<InstructionUnit> units) {
		for(InstructionUnit unit : units) {
			Terminal terminal = null;
			Token token = null;
			if(unit.getType() >= 100) {
				terminal = (Terminal) unit;
			} else {
				token = (Token) unit;
			}
			if(token == null) {
				if(terminal.getType() == Terminal.CONDITION) {
					execute(((Terminal)terminal.getUnit(4)).getUnits());
				} else if(terminal.getType() == Terminal.BODY || terminal.getType() == Terminal.STATEMENT) {
					execute(terminal.getUnits());
				} else if(terminal.getType() == Terminal.VARIABLE_INIT) {
					if(terminal.getUnit(1).getType() == Terminal.ASSIGNMENT) {
						if(((Terminal) terminal.getUnit(1)).getUnit(2).getType() == Token.VARIABLE || ((Terminal) terminal.getUnit(1)).getUnit(2).getType() == Token.CONSTANT) {
							intVars.put(((Terminal)terminal.getUnit(1)).getUnit(0).getValue(), Integer.parseInt(((Terminal)terminal.getUnit(1)).getUnit(2).getValue()));
							System.out.println(((Terminal)terminal.getUnit(1)).getUnit(0).getTypeName() + " \"" + ((Terminal)terminal.getUnit(1)).getUnit(0).getValue() +
									"\" was initialized with value of " + ((Terminal)terminal.getUnit(1)).getUnit(2).getValue());
						} else if(((Terminal) terminal.getUnit(1)).getUnit(2).getType() == Terminal.MATH) {
							ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
							math.add(((Terminal) terminal.getUnit(1)).getUnit(2));
							execute(math);
							intVars.put(((Terminal)terminal.getUnit(1)).getUnit(0).getValue(), mathRes.get(((Terminal) terminal.getUnit(1)).getUnit(2)));
							System.out.println(((Terminal)terminal.getUnit(1)).getUnit(0).getTypeName() + " \"" + ((Terminal)terminal.getUnit(1)).getUnit(0).getValue() +
									"\" was initialized with value of " + mathRes.get(((Terminal) terminal.getUnit(1)).getUnit(2)));
						} else if(((Terminal) terminal.getUnit(1)).getUnit(2).getType() == Terminal.BOOLEAN) {
							ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
							bool.add(((Terminal) terminal.getUnit(1)).getUnit(2));
							execute(bool);
							boolVars.put(((Terminal)terminal.getUnit(1)).getUnit(0).getValue(), boolRes.get(((Terminal) terminal.getUnit(1)).getUnit(2)));
							System.out.println(((Terminal)terminal.getUnit(1)).getUnit(0).getTypeName() + " \"" + ((Terminal)terminal.getUnit(1)).getUnit(0).getValue() +
									"\" was initialized with value of " + boolRes.get(((Terminal) terminal.getUnit(1)).getUnit(2)));
						}
					} else {
						intVars.put(terminal.getUnit(1).getValue(), 0);
						System.out.println(terminal.getUnit(1).getTypeName() + " \"" + terminal.getUnit(1).getValue() +	"\" was initialized with value of ZERO");
					}
					break;
				} else if(terminal.getType() == Terminal.ASSIGNMENT) {
					int type = 0;
					if(intVars.get(terminal.getUnit(0).getValue()) != null) {
						type = Token.INT;
					} else if(boolVars.get(terminal.getUnit(0).getValue()) != null) {
						type = Token.BOOL;
					} else {
						error("Variable \"" + terminal.getUnit(0).getValue() + "\" is undefined");
					}
					if(type == Token.INT) {
						if(terminal.getUnit(2).getType() == Terminal.MATH) {
							ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
							math.add(terminal.getUnit(2));
							execute(math);
							System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + intVars.get(terminal.getUnit(0).getValue()) + " to " +  mathRes.get(terminal.getUnit(2)));
							intVars.put(terminal.getUnit(0).getValue(), mathRes.get(terminal.getUnit(2)));
						} else if(terminal.getUnit(2).getType() == Token.VARIABLE) {
							if(intVars.get(terminal.getUnit(2).getValue()) != null) {
								System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + intVars.get(terminal.getUnit(0).getValue()) + " to " +  intVars.get(terminal.getUnit(2).getValue()));
								intVars.put(terminal.getUnit(0).getValue(), intVars.get(terminal.getUnit(2).getValue()));
							} else {
								error("Variable \"" + terminal.getUnit(2).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(2).getType() == Token.CONSTANT) {
							System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + intVars.get(terminal.getUnit(0).getValue()) + " to " +  terminal.getUnit(2).getValue());
							intVars.put(terminal.getUnit(0).getValue(), Integer.parseInt(terminal.getUnit(2).getValue()));
						}
					} else if(type == Token.BOOL) {
						if(terminal.getUnit(2).getType() == Terminal.BOOLEAN) {
							ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
							bool.add(terminal.getUnit(2));
							execute(bool);
							System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + boolVars.get(terminal.getUnit(0).getValue()) + " to " +  boolRes.get(terminal.getUnit(2)));
							boolVars.put(terminal.getUnit(0).getValue(), boolRes.get(terminal.getUnit(2)));
						} else if(terminal.getUnit(2).getType() == Token.VARIABLE) {
							if(boolVars.get(terminal.getUnit(2).getValue()) != null) {
								System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + boolVars.get(terminal.getUnit(0).getValue()) + " to " +  boolVars.get(terminal.getUnit(2).getValue()));
								boolVars.put(terminal.getUnit(0).getValue(), boolVars.get(terminal.getUnit(2).getValue()));
							} else {
								error("Variable \"" + terminal.getUnit(2).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(2).getType() == Token.TRUE || terminal.getUnit(2).getType() == Token.FALSE) {
							System.out.println("Changing \"" + terminal.getUnit(0).getValue() + "\" from " + boolVars.get(terminal.getUnit(0).getValue()) + " to " +  terminal.getUnit(2).getValue());
							boolVars.put(terminal.getUnit(0).getValue(), Boolean.parseBoolean(terminal.getUnit(2).getValue()));
						}
					}
				} else if(terminal.getType() == Terminal.METHOD) {
					
				} else if(terminal.getType() == Terminal.MATH) {
					System.out.println("-----SOLVING-----");
					System.out.println(terminal.getValue());
					int res = 0, a = 0, b = 0;
					if(terminal.getUnit(1).getType() != Terminal.MATH && terminal.getUnit(1).getType() != Token.CONSTANT) {
						if(terminal.getUnit(0).getType() == Terminal.MATH) {
							ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
							math.add(((Terminal)terminal.getUnit(0)));
							execute(math);
						}
						if(terminal.getUnit(2).getType() == Terminal.MATH) {
							ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
							math.add(((Terminal)terminal.getUnit(2)));
							execute(math);
						}
						//A
						if(terminal.getUnit(0).getType() == Token.VARIABLE) {
							a = intVars.get(terminal.getUnit(0).getValue());
						} else if(terminal.getUnit(0).getType() == Token.CONSTANT) {
							a = Integer.parseInt(terminal.getUnit(0).getValue());
						} else if(terminal.getUnit(0).getType() == Terminal.MATH) {
							a = mathRes.get(terminal.getUnit(0));
						}
						//B
						if(terminal.getUnit(2).getType() == Token.VARIABLE) {
							b = intVars.get(terminal.getUnit(2).getValue());
						} else if(terminal.getUnit(2).getType() == Token.CONSTANT) {
							b = Integer.parseInt(terminal.getUnit(2).getValue());
						} else if(terminal.getUnit(2).getType() == Terminal.MATH) {
							b = mathRes.get(terminal.getUnit(2));
						}
						//Math
						if(terminal.getUnit(1).getValue().contains("+")) {
							res = a + b;
							System.out.println(a + " + " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("-")) {
							res = a - b;
							System.out.println(a + " - " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("*")) {
							res = a * b;
							System.out.println(a + " * " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("/")) {
							if(b != 0) {
								res = a / b;
								System.out.println(a + " / " + b + " = " + res);
							} else {
								error("Division by zero");
							}
						} else if(terminal.getUnit(1).getValue().contains("^")) {
							res = (int) Math.pow(a, b);
							System.out.println(a + " ^ " + b + " = " + res);
						}
					} else if(terminal.getUnit(0).getType() == Token.LEFT_BRACKET && terminal.getUnit(1).getType() == Terminal.MATH) {
						ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
						math.add(((Terminal)terminal.getUnit(1)));
						execute(math);
						res = mathRes.get(terminal.getUnit(1));
					} else if(terminal.getUnit(1).getType() == Terminal.MATH) {
						ArrayList<InstructionUnit> math = new ArrayList<InstructionUnit>(1);
						math.add(((Terminal)terminal.getUnit(1)));
						execute(math);
						res = mathRes.get(terminal.getUnit(1)) * (terminal.getUnit(0).getValue().contains("+") ? 1 : -1);
						System.out.println("Applying sign: " + res);
					} else if(terminal.getUnit(1).getType() == Token.CONSTANT) {
						res = Integer.parseInt(terminal.getUnit(1).getValue()) * (terminal.getUnit(0).getValue().contains("+") ? 1 : -1);
					}  else {
						error("Math can't be solved");
					}
					mathRes.put(terminal, res);
				} else if(terminal.getType() == Terminal.BOOLEAN) {
					System.out.println("-----SOLVING-----");
					System.out.println(terminal.getValue());
					boolean res = false, a = false, b = false;
					int numA = 0, numB = 0;
					
					if(terminal.getUnit(1).getType() != Terminal.BOOLEAN && terminal.getUnit(1).getType() != Token.TRUE && terminal.getUnit(1).getType() != Token.FALSE) {
						if(terminal.getUnit(0).getType() == Terminal.BOOLEAN) {
							ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
							bool.add(((Terminal)terminal.getUnit(0)));
							execute(bool);
						}
						if(terminal.getUnit(2).getType() == Terminal.BOOLEAN) {
							ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
							bool.add(((Terminal)terminal.getUnit(2)));
							execute(bool);
						}
						//A
						if(terminal.getUnit(0).getType() == Token.VARIABLE) {
							if(boolVars.get(terminal.getUnit(0).getValue()) != null) {
								a = boolVars.get(terminal.getUnit(0).getValue());
							} else {
								error("Variable \"" + terminal.getUnit(0).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(0).getType() == Token.TRUE || terminal.getUnit(0).getType() == Token.FALSE) {
							a = Boolean.parseBoolean(terminal.getUnit(0).getValue());
						} else if(terminal.getUnit(0).getType() == Terminal.BOOLEAN) {
							a = boolRes.get(terminal.getUnit(0));
						}
						//B
						if(terminal.getUnit(2).getType() == Token.VARIABLE) {
							if(boolVars.get(terminal.getUnit(2).getValue()) != null) {
								b = boolVars.get(terminal.getUnit(2).getValue());
							} else {
								error("Variable \"" + terminal.getUnit(2).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(2).getType() == Token.TRUE || terminal.getUnit(2).getType() == Token.FALSE) {
							b = Boolean.parseBoolean(terminal.getUnit(2).getValue());
						} else if(terminal.getUnit(2).getType() == Terminal.BOOLEAN) {
							b = boolRes.get(terminal.getUnit(2));
						}
						//Boolean
						if(terminal.getUnit(1).getValue().contains("&&")) {
							res = a && b;
							System.out.println(a + " && " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("||")) {
							res = a || b;
							System.out.println(a + " || " + b + " = " + res);
						}
						
						//Compare
						//Num A
						if(terminal.getUnit(0).getType() == Token.CONSTANT) {
							numA = Integer.parseInt(terminal.getUnit(0).getValue());
						} else if(terminal.getUnit(0).getType() == Token.VARIABLE) {
							if(intVars.get(terminal.getUnit(0).getValue()) != null) {
								numA = intVars.get(terminal.getUnit(0).getValue());
							} else {
								error("Variable \"" + terminal.getUnit(0).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(0).getType() == Terminal.MATH) {
							numA = Integer.parseInt(terminal.getUnit(0).getValue());
						}
						//Num B
						if(terminal.getUnit(2).getType() == Token.CONSTANT) {
							numB = Integer.parseInt(terminal.getUnit(2).getValue());
						} else if(terminal.getUnit(2).getType() == Token.VARIABLE) {
							if(intVars.get(terminal.getUnit(2).getValue()) != null) {
								numA = intVars.get(terminal.getUnit(2).getValue());
							} else {
								error("Variable \"" + terminal.getUnit(2).getValue() + "\" is undefined");
							}
						} else if(terminal.getUnit(2).getType() == Terminal.MATH) {
							numB = Integer.parseInt(terminal.getUnit(2).getValue());
						}
						//Boolean
						if(terminal.getUnit(1).getValue().contains(">")) {
							res = numA > numB;
							System.out.println(numA + " > " + numB + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains(">=")) {
							res = numA >= numB;
							System.out.println(a + " >= " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("<")) {
							res = numA < numB;
							System.out.println(a + " < " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("<=")) {
							res = numA <= numB;
							System.out.println(a + " <= " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("==")) {
							res = numA == numB;
							System.out.println(a + " == " + b + " = " + res);
						} else if(terminal.getUnit(1).getValue().contains("!=")) {
							res = numA != numB;
							System.out.println(a + " != " + b + " = " + res);
						}
					} else if(terminal.getUnit(0).getType() == Token.LEFT_BRACKET && terminal.getUnit(1).getType() == Terminal.BOOLEAN) {
						ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
						bool.add(((Terminal)terminal.getUnit(1)));
						execute(bool);
						res = boolRes.get(terminal.getUnit(1));
					} else if(terminal.getUnit(1).getType() == Terminal.BOOLEAN) {
						ArrayList<InstructionUnit> bool = new ArrayList<InstructionUnit>(1);
						bool.add(((Terminal)terminal.getUnit(1)));
						execute(bool);
						res = !boolRes.get(terminal.getUnit(1));
						System.out.println("Applying sign: " + res);
					} else if(terminal.getUnit(1).getType() == Token.TRUE || terminal.getUnit(1).getType() == Token.FALSE) {
						res = !Boolean.parseBoolean(terminal.getUnit(1).getValue());
					}  else {
						error("Boolean can't be solved");
					}
					boolRes.put(terminal, res);
				}
			} else {
				
			}
		}
	}
	
	private void error(String err) {
		System.out.println("ERROR");
		System.out.println(err);
		System.exit(0);
	}
}
