package root;

public interface InstructionUnit {
	
	public int getType();
	public String getTypeName();
	public String getValue();
}
